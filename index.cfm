<cfinclude template="head.cfm">
<cfoutput>
		<body class="nav-active">
			<!-- main container of all the page elements -->
			<cfquery name="get_jobs" datasource="#application.db#">
		             SELECT *
		             FROM jobs
		    </cfquery>
			<div id="wrapper">
				<!-- header of the page -->
				<cfinclude template="header.cfm">
				<div id="main" role="main">
					<div class="main-holder">
						<div class="task-information">
							<div class="container">
										<div id="details">
										
									    </div>
									</div>
							<div class="container-fluid">
								<div class="row">
									<!-- task section -->
									<!--- <div class="container">
										<div id="details" style="padding-left: 23%">
										
									    </div>
									</div> --->
								<!-- navigation panel -->
									<div class="left-panel">
										 <a href="##" class="nav-opener text-uppercase"><span></span></a>
										 <div class="heading-wrap">
											  <strong class="panel-title text-capitalize">Projects</strong>
										 </div>
									     <div id="jobs" style="cursor: pointer;">
									    	 <i class="fa fa-list-alt" ></i> <strong> Job Setup</strong>
									     </div>
										 <div class="clear-10"></div>
										 <div id="lists-holder" style="display: none; cursor: pointer;" >	
											  <ul>
												<cfloop query="get_jobs">
						      						    <li id="#jobID#"  onclick="showDetails(#jobID#)"> Job #Name#</li>
											    </cfloop>
											  </ul>
											  <div style="margin-left: 24px;">
											       <strong  onclick="addJob()"> <i class="fa fa-plus-circle" style="padding-right:6px"></i>Add New Job</strong>
										      </div>
										 </div>
						            </div>
				                </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<link rel="stylesheet" type="text/css" href="css/tasks.css" defer></link>
			<script src="js/tasks.js" type="text/javascript"></script>
			<footer></footer>
			<cfparam name="url.js" default="">
				<cfif url.js gt "">
					<script>
						$(function(){
							setTimeout(function () {
						       #url.js#
						    }, 2000);
							
						});
					</script>
				</cfif>
		</body>
</cfoutput>