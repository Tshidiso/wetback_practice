$(function(argument) {
	initFeed('performance_manager.cfm');
	initBodyClicks();
	getPerformanceAssesments();
	initModals();
});
function showLoading(){
	$("#performance-holder")['0'].innerHTML = "<div class='loading-holder'> <img src='images/loading.gif' id='loading'/></div>";
}
function showLoadingDiv(div_id){
	$("#"+div_id)['0'].innerHTML = "<div class='loading-holder2'> <img src='images/loading.gif' id='loading2'/></div>";
}
/*   SaveAssessmentAnswers */
function SaveAssessmentAnswers(EmployeeAssessmentID,isManager,AdminID,HolderClass,ObjectiveTypeID,IncAdminID){
	performancesummary = "";
	if(ObjectiveTypeID == "1" && isManager == "1"){
		performancesummary = $('#performancesummary-procedural-'+EmployeeAssessmentID)['0'].value;
	}else if(ObjectiveTypeID == "2" && isManager == "1"){
		performancesummary =$('#performancesummary-strategic-'+EmployeeAssessmentID)['0'].value;
	}

	var jsonData = {};
	var assessmentAnswers = [];
	jsonData.assessmentAnswers = assessmentAnswers;
	if(isManager == "0"){
		$("."+HolderClass+" .employee-rating").each(function(i,el){
			measureid = $(el).data('measureid');
			comment = $("#employee_comment"+EmployeeAssessmentID+"_"+measureid)['0'].value;
			var assessmentAnswer = {
			    "measureid": measureid,
			    "ratingid":$(el)['0'].value,
			    "dev_required":"0",
			    "manager_comment":comment
			}
			jsonData.assessmentAnswers.push(assessmentAnswer);
		});
	}else{
		$("."+HolderClass+" .manager-rating").each(function(i,el){
			measureid = $(el).data('measureid')
			devRequiredCheckbox = $("#DevRequired"+EmployeeAssessmentID+"-"+measureid);
			if(devRequiredCheckbox[0].checked){
				dev_required = "1";
			}else{
				dev_required = "0";
			}
			manager_comment = $("#manager_comment"+EmployeeAssessmentID+"_"+measureid)['0'].value;
			var assessmentAnswer = {
			    "measureid":measureid ,
			    "manager_comment":manager_comment,
			    "ratingid":$(el)['0'].value,
			    "dev_required":dev_required
			}
			jsonData.assessmentAnswers.push(assessmentAnswer);
		});
	}
	console.log(JSON.stringify(jsonData));

	$.ajax({
		method: "Post",
		url: "ajax/performance/save_assessment_answers.cfm",
		data:{ 
			assessmentAnswers:JSON.stringify(jsonData),
			EmployeeAssessmentID:EmployeeAssessmentID,
			isManager:isManager,
			ObjectiveTypeID:ObjectiveTypeID,
			PerformanceSummary:performancesummary
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
			$("#AdminToggel"+IncAdminID).removeClass("activated");
    		getEmployeeAssessments(IncAdminID,EmployeeAssessmentID,ObjectiveTypeID)
        }
	});
}
function showObjectiveHolder(id,el) {
	$(".objectives-holder").hide();
	$(".objectives-title").removeClass("active");
	$("#"+id).show();
	$(el).addClass("active");
}

function showObjectiveHolder2(id,el,assessmentid) {
	$(".obj-hold"+assessmentid).hide();
	$(".inner-obj"+assessmentid).removeClass("active");
	$("#"+id+""+assessmentid).show();
	$(el).addClass("active");
}
function showObjHolder(target_class,EmployeeAssessmentID,el) {
	$(".employee-assessment-objectives"+EmployeeAssessmentID).hide();
	$(".obj-type-holder"+EmployeeAssessmentID).removeClass("active");
	$("."+target_class+""+EmployeeAssessmentID).show();
	$(el).addClass("active");
}
function showImprovementHolder(target_class,EmployeeAssessmentID,el) {
	$(".improvement-employee-assessment-objectives"+EmployeeAssessmentID).hide();
	$(".improvement-type-holder"+EmployeeAssessmentID).removeClass("active");
	$("."+target_class+""+EmployeeAssessmentID).show();
	$(el).addClass("active");
}

function showObjHolder2(target_class,EmployeeAssessmentID,el) {
	$(".employee-assessment-objectives2"+EmployeeAssessmentID).hide();
	$(".obj-type-holder2"+EmployeeAssessmentID).removeClass("active");
	$("."+target_class+""+EmployeeAssessmentID).show();
	$(el).addClass("active");
}
/*   SaveAssessmentAnswers */

/*  CurrentAssesments  */ 
function getCurrentAssesments(){
	showLoading();
	$.ajax({
		method: "GET",
		url: "ajax/performance/current_assessments.cfm"
	}) .done(function( data ) {
		clearActiveSecondNavItems();
		$("#CurrentAssesmentsLink").addClass("active");
        $("#performance-holder")['0'].innerHTML = '';
        $("#performance-holder")['0'].innerHTML = data;
	});
}
/*  CurrentAssesments  */ 

/*  getPerformanceAssesments  */ 
function getPerformanceAssesments(AdminID,EmployeeAssessmentID,ObjectiveTypeID){
	if(AdminID === undefined){
	    AdminID = '';
	}
	if(EmployeeAssessmentID === undefined){
	    EmployeeAssessmentID = '';
	}
	if(ObjectiveTypeID === undefined){
	    ObjectiveTypeID = '';
	}
	if( $("#SearchEmployee").length == 1){
	  	SearchEmployee = $("#SearchEmployee")['0'].value;
		SearchAssessmentPhases = '';
		SearchJobTitle= $("#SearchJobTitle")['0'].value;
		SearchManager= $("#SearchManager")['0'].value;
		SearchPerformance= $("#SearchPerformance")['0'].value;
	    SearchCompleteAssessmentStatus = $("#SearchAssessmentStatus")['0'].value;
	    SearchDateTo = $("#SearchDateTo")['0'].value;
	    SearchDateFrom = $("#SearchDateFrom")['0'].value;
	    SearchAssessmentStatus = '';
	}else{
    	SearchEmployee = '';
		SearchAssessmentPhases = '';
	    SearchAssessmentStatus = '';
	    SearchCompleteAssessmentStatus = '';
	    SearchJobTitle= '';
		SearchManager= '';
		SearchPerformance= '';
		SearchDateTo= '';
		SearchDateFrom= '';
    }

	showLoading();
	$.ajax({
		method: "GET",
		url: "ajax/performance/performance_assessments.cfm?AdminID="+AdminID+"&EmployeeAssessmentID="+EmployeeAssessmentID+"&ObjectiveTypeID="+ObjectiveTypeID+"&SearchEmployee="+SearchEmployee+"&SearchAssessmentPhases="+SearchAssessmentPhases+"&SearchAssessmentStatus="+SearchAssessmentStatus+"&SearchCompleteAssessmentStatus="+SearchCompleteAssessmentStatus+"&SearchJobTitle="+SearchJobTitle+"&SearchManager="+SearchManager+"&SearchPerformance="+SearchPerformance+"&SearchDateTo="+SearchDateTo+"&SearchDateFrom="+SearchDateFrom
	}) .done(function( data ) {
		clearActiveSecondNavItems();
		$("#PerformanceAssesmentsLink").addClass("active");
        $("#performance-holder")['0'].innerHTML = '';
        $("#performance-holder")['0'].innerHTML = data;
        initDatePicker2();
        if(SearchEmployee != ''){
			getEmployeeAssessments(SearchEmployee);
		}
	});
}

function getEmployeeAssessments(AdminID,EmployeeAssessmentID,ObjectiveTypeID,ImprovementAssessment){
	div_id = "AdminToggel"+AdminID;
	if($("#"+div_id).hasClass("activated")){
		$("#"+div_id).slideUp();
		$("#"+div_id).removeClass("activated");
        $("#"+div_id)['0'].innerHTML = '';
		return;
	}
	if(AdminID === undefined){
	    AdminID = '';
	}
	if(EmployeeAssessmentID === undefined){
	    EmployeeAssessmentID = '';
	}
	if(ObjectiveTypeID === undefined){
	    ObjectiveTypeID = '';
	}
	if(ImprovementAssessment == undefined){
		ImprovementAssessment = '';
	}
	if( $("#SearchEmployee").length == 1){
	  	SearchEmployee = $("#SearchEmployee")['0'].value;
		SearchAssessmentPhases = '';
		SearchJobTitle= $("#SearchJobTitle")['0'].value;
		SearchManager= $("#SearchManager")['0'].value;
		SearchPerformance= $("#SearchPerformance")['0'].value;
	    SearchCompleteAssessmentStatus = $("#SearchAssessmentStatus")['0'].value;
	    SearchAssessmentStatus = '';
	    SearchDateTo = $("#SearchDateTo")['0'].value;
	    SearchDateFrom = $("#SearchDateFrom")['0'].value;
	}else{
    	SearchEmployee = '';
		SearchAssessmentPhases = '';
	    SearchAssessmentStatus = '';
	    SearchCompleteAssessmentStatus = '';
	    SearchJobTitle= '';
		SearchManager= '';
		SearchPerformance= '';
		SearchDateTo= '';
		SearchDateFrom= '';
    }
	showLoadingDiv(div_id);
	$("#"+div_id).slideDown();
	$.ajax({
		method: "GET",
		url: "ajax/performance/performance_assessment/get_employee_assessment.cfm?AdminID="+AdminID+"&EmployeeAssessmentID="+EmployeeAssessmentID+"&ObjectiveTypeID="+ObjectiveTypeID+"&ImprovementAssessment="+ImprovementAssessment+"&SearchEmployee="+SearchEmployee+"&SearchAssessmentPhases="+SearchAssessmentPhases+"&SearchAssessmentStatus="+SearchAssessmentStatus+"&SearchCompleteAssessmentStatus="+SearchCompleteAssessmentStatus+"&SearchJobTitle="+SearchJobTitle+"&SearchManager="+SearchManager+"&SearchPerformance="+SearchPerformance+"&SearchDateTo="+SearchDateTo+"&SearchDateFrom="+SearchDateFrom
	}) .done(function( data ) {
		$("#"+div_id).addClass("activated");
        $("#"+div_id)['0'].innerHTML = '';
        $("#"+div_id)['0'].innerHTML = data;
        if($("#chartY"+AdminID).length == 1){
       	 showChart(AdminID);
        }
      	initDatePicker3();
	});
}

function addEmployeeAssessment(AdminID,AssessmentID){
	$("#assessments-holder-list"+AdminID).hide();
	$.ajax({
		method: "Post",
		url: "ajax/performance/add_employee_assessment.cfm",
		data:{ 
			AdminID:AdminID,
			AssessmentID:AssessmentID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
        	
        	$(".PerformanceScoreToggel"+AdminID).show();
			$("#AdminToggel"+AdminID).removeClass("activated");
			getEmployeeAssessments(AdminID);
		}
	});
}	
/*  EmployeeAssesments  */ 

/*  CreateAssesment  */ 
function getCreateAssesment(AssessmentID){
	if(AssessmentID === undefined){
	    AssessmentID = '';
	}
	showLoading();
	$.ajax({
		method: "GET",
		url: "ajax/performance/create_assessment.cfm?AssessmentID="+AssessmentID
	}) .done(function( data ) {
		clearActiveSecondNavItems();
		$("#CreateAssesmentLink").addClass("active");
        $("#performance-holder")['0'].innerHTML = '';
        $("#performance-holder")['0'].innerHTML = data;
	});
}
function addObjective(){
	if($("#objectives-holder").hasClass("not-active")){
		$("#objectives-holder").removeClass("not-active");
		$("#objectives-holder").slideDown();
	}else{
		$.ajax({
			method: "GET",
			url: "ajax/performance/create_assessment_new_objective.cfm?Count="+$('.objective').length
		}) .done(function( data ) {
			$("#objectives-holder").append(data);
		});
	}
}
function addMeasure(ObjectiveNo){
	if($("#measure-holder"+ObjectiveNo).hasClass("not-active")){
		$("#measure-holder"+ObjectiveNo).removeClass("not-active");
		$("#measure-holder"+ObjectiveNo).slideDown();
	}
	$.ajax({
		method: "GET",
		url: "ajax/performance/create_assessment_new_measure.cfm?Count="+$("#measure-holder"+ObjectiveNo+" .measure").length+"&ObjectiveNo="+ObjectiveNo
	}) .done(function( data ) {
		$("#measure-holder"+ObjectiveNo).append(data);
	});
}



function UpdateObjectiveWeighting(ObjectiveNo){
	weighting = 0;
	$("#create-assessment-holder #objective"+ObjectiveNo+" .measure-weighting").each(function(i,el){
		if($(el)['0'].value != "" && $(el)['0'].value > 0){
			weighting += $(el)['0'].value-0;
		}
	});
	$("#objectiveWeighting"+ObjectiveNo)['0'].value = weighting;
	return weighting;
}

function SaveProcedualObjectivesForm(elem){
	$(elem).hide();
	$("#pleaseWait").show();
	AssessmentName = $("#AssessmentName")['0'].value;
	message = "";
	if(AssessmentName == ""){
		message = "Please enter the assessment's name";
		$("#AssessmentName").focus();
	}
	if(message == ""){
		weighting = 0;
		$("#create-assessment-holder .objective").each(function(i,el){
			ObjectiveNo = $(el).data('no');
			if(message == ""){
				message = checkObjective(ObjectiveNo);
			}
			if(message == ""){
				weighting += UpdateObjectiveWeighting(ObjectiveNo);
			}
		});
		if(weighting != 100){
			message = "Weighting needs to add up to 100%";
			$(".ipa-objectives-title").click();
		}
	}
	if(message != ""){
		showMessage("<b>Error:</b> "+message,"error");
		$(elem).show();
		$("#pleaseWait").hide();
	}else{
		var jsonData = {};
		var objects = [];
		var strategic_objectives = [];
		jsonData.objects = objects;
		$("#create-assessment-holder .objective").each(function(i,el){
			ObjectiveNo = $(el).data('no');
			var object = {
			    "no": ObjectiveNo,
			    "name":$("#objective"+ObjectiveNo+" .objective-name")['0'].value,
			    "weighting":$("#objective"+ObjectiveNo+" .objective-weighting")['0'].value,
			    "type":"1"
			}
			jsonData.objects.push(object);
			var measures = [];
			jsonData.objects[i].measures = measures;
			$("#create-assessment-holder #objective"+ObjectiveNo+" .measure").each(function(i2,el2){
				MeasureNo = $(el2).data('no');
				measure_name = "";
				measure_weighting = "";
				measure_rating = "";
				if($("#measure"+ObjectiveNo+""+MeasureNo).length == 1){
					measure_name = $("#measure"+ObjectiveNo+""+MeasureNo+" .measure-name")['0'].value;
					measure_weighting = $("#measure"+ObjectiveNo+""+MeasureNo+" .measure-weighting")['0'].value;
					measure_rating = $("#measure"+ObjectiveNo+""+MeasureNo+" .measure-rating")['0'].value;
				}
				var measure = {
				    "no": MeasureNo,
				    "measure_name":measure_name,
				    "measure_weighting": measure_weighting,
				    "measure_rating":measure_rating
				}
				jsonData.objects[i].measures.push(measure);
			});
		});

		$("#strategic-objectives-holder .stratigic-measure").each(function(i,el){
			objectiveid = $(el).data('objectiveid');
			measureid = $(el).data('measureid');
			target = $(el)['0'].value;
			var strategic_objective = {
			    "objectiveid": objectiveid,
			    "measureid":measureid,
			    "target":target,
			    "type":"2"
			}
			jsonData.objects.push(strategic_objective);
		});

	    $.ajax({
			method: "Post",
			url: "ajax/performance/save_assessment.cfm",
			data:{ 
				Objects:JSON.stringify(jsonData),
				AssessmentName:AssessmentName
			}
		}).done(function( data ) {
			response = JSON.parse(data);
            success = response.success;
            message = response.message;
            if(success == "0"){
        		$(".ipa-objectives-title").click();
        		showMessage("<b>Error:</b> "+message,"error");
        		$(elem).show();
        		$("#pleaseWait").hide();

            }else{
        		showMessage("<b>Success:</b> "+message,"success");
        		getCreateAssesment(response.assessmentid);
            }
		});
	}
}


function SaveImprovementPlan(EmployeeAssessmentID,AdminID,HolderClass){
	performancesummary = "";
	var jsonData = {};
	var assessmentAnswers = [];
	jsonData.assessmentAnswers = assessmentAnswers;
	$("."+HolderClass+" .developmentstrategy").each(function(i,el){
		asssessmentanswerid = $(el).data('asssessmentanswerid');
		var assessmentAnswer = {
		    "asssessmentanswerid": $(el).data('asssessmentanswerid'),
		    "objectivetypeid": $(el).data('objectivetypeid'),
		    "developmentstrategy":$(el)['0'].value,
		    "improvementsource":$(".improvementsource"+asssessmentanswerid)['0'].value
		}
		jsonData.assessmentAnswers.push(assessmentAnswer);
	});
	console.log(JSON.stringify(jsonData));
	
	$.ajax({
		method: "Post",
		url: "ajax/performance/save_improvement_answers.cfm",
		data:{ 
			assessmentAnswers:JSON.stringify(jsonData),
			EmployeeAssessmentID:EmployeeAssessmentID,
			AdminID:AdminID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
			$("#AdminToggel"+AdminID).removeClass("activated");
    		getEmployeeAssessments(AdminID,EmployeeAssessmentID);
        }
	});
}

function removeMeasure(el,ObjectiveNo){
	$(el).parent().parent().slideUp( 'slow', function() {
		$(this).remove();
		recountMeasures(ObjectiveNo);
		UpdateObjectiveWeighting(ObjectiveNo);
	});
}
function recountMeasures(ObjectiveNo){
	$("#create-assessment-holder #objective"+ObjectiveNo+" .measure").each(function(i,el){
		newMeasureNo = i+1;
		MeasureNo = $(el).data('no');
		$(el).data('no',newMeasureNo);
		$(el).attr('data-no',newMeasureNo);
		$(el).attr("id","measure"+ObjectiveNo+""+newMeasureNo);
		measureLabel = $("#measure-label"+ObjectiveNo+""+MeasureNo);
		measureLabel['0'].innerHTML = newMeasureNo+".";
		measureLabel.attr("id","measure-label"+ObjectiveNo+""+newMeasureNo);

	});
}
function checkObjective(ObjectiveNo){
	message = "";
	$("#create-assessment-holder #objective"+ObjectiveNo+" .measure-weighting").each(function(i,el){
		if(message == ""){
			if($(el)['0'].value == "" || $(el)['0'].value < 0){
				$(".ipa-objectives-title").click();
				$(el).focus();
				message = "Please fill in all measure's weighting's";
			}
		}
	});

	return message;
}
/*  CreateAssesment  */ 

/*  Search  */ 
function searchAssessments(){
	getPerformanceAssesments('','','');
}
/*  Search  */ 

function showChart(AdminID){
	phases = $("#chartY"+AdminID).data("phases");
	var phases_array = phases.split(',');
	overall = $("#chartOverall"+AdminID).data("overall");
	var overall_array = overall.split(',').map(Number);
	current = $("#chartX"+AdminID).data("current");
	var current_array = current.split(',').map(Number);
	console.log(current_array);
	console.log(overall_array);
	
   	Highcharts.chart('chartContainer'+AdminID, {
        title: {
            text: 'Assessment Summary',
            x: -20 //center
        },
        xAxis: {

            categories: phases_array
        },
        yAxis: {
            title: {
                text: ''
            },
            min: 1,
        	softMax:5,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Overall Rating',
            data: overall_array,
            color: '#2a82bf'
        }, {
            name: 'Current Assessment',
            data: current_array,
            color: '#08ce08',
            dashStyle: 'longdash'
        }	]
    });
}

// Improvement Plan
// function addImprovementObjective(){
// 	if($("#objectives-holder").hasClass("not-active")){
// 		$("#objectives-holder").removeClass("not-active");
// 		$("#objectives-holder").slideDown();
// 	}else{
// 		$.ajax({
// 			method: "GET",
// 			url: "ajax/performance/create_assessment_new_objective.cfm?Count="+$('.objective').length
// 		}) .done(function( data ) {
// 			$("#objectives-holder").append(data);
// 		});
// 	}
// }

function addImprovementObjective(ObjectiveID,EmployeeAssessmentID){
	$.ajax({
		method: "GET",
		url: "ajax/performance/performance_improvement/create_improvement_objective.cfm?EmployeeAssessmentID="+EmployeeAssessmentID
	}) .done(function( data ) {
		$(data).insertAfter($("#add_objective_holder"+ObjectiveID+"_"+EmployeeAssessmentID));
	});
}

function addImprovementMeasure(ObjectiveID,EmployeeAssessmentID,MeasureID){
	measures_count = $("#objective_"+ObjectiveID+"_"+EmployeeAssessmentID+" .measure_row").length;

	$.ajax({
		method: "GET",
		url: "ajax/performance/performance_improvement/create_improvement_measure.cfm?EmployeeAssessmentID="+EmployeeAssessmentID
	}) .done(function( data ) {
		$("#measure_holder"+MeasureID+"_"+EmployeeAssessmentID).append(data);
	});
}

function addImprovementSubMeasure(MeasureID,EmployeeAssessmentID){
	submeasures_count = $("#measure_table_"+MeasureID+"_"+EmployeeAssessmentID+" .submeasure_row").length;
	$.ajax({
		method: "GET",
		url: "ajax/performance/performance_improvement/create_improvement_sub_measure.cfm?EmployeeAssessmentID="+EmployeeAssessmentID+"&Count="+submeasures_count
	}) .done(function( data ) {
		// $("#measure-holder"+ObjectiveNo).append(data);
		if(submeasures_count > 0){
			$(data).insertAfter($("#measure_table_"+MeasureID+"_"+EmployeeAssessmentID+" .submeasure_row")[submeasures_count-1]);
		}else{
			$(data).insertAfter($("#measure_"+MeasureID+"_"+EmployeeAssessmentID));
		}
	});
}

function saveImprovementPlan(AdminID,EmployeeAssessmentID,AssessmentPhaseID,ObjectiveTypeID){
	var jsonData = {};
	var objectives = [];
	jsonData.objectives = objectives;
	objectives_el = $("#improvement_plan"+EmployeeAssessmentID+" .improvement_objective");
	
	objectives_el.each(function(i,el){
		objectiveid = $(el).data('objectiveid');
		objectivenew = $(el).data('objectivenew');
		if(objectivenew == "0"){
			objectivename = $(el).data('objectivename');
			objectiveweighting = $("#objectiveweighting_"+objectiveid+"_"+EmployeeAssessmentID)['0'].value;
		}else{
			objectivename = $("#objectivename_"+objectiveid)['0'].value;
			objectiveweighting = $("#objectiveweighting_"+objectiveid)['0'].value;
		}
		var objective = {
		    "objectiveid": $(el).data('objectiveid'),
		    "objectivename": objectivename,
		    "objectivenew": objectivenew,
		    "objectiveweighting": objectiveweighting
		}

		var measures = []; 
		objective.measures = measures;
		measures_el = $("#objective_"+objectiveid+"_"+EmployeeAssessmentID+" .improvement_measure");
		measures_el.each(function(i,measure_el){
			measureid = $(measure_el).data('measureid');
			measurenew = $(measure_el).data('measurenew');
			if(measurenew == "0"){
				measurename = $(measure_el).data('measurename');
				measuretarget = $(measure_el).data('measuretarget');
				measureweighting = $("#measureweighting_"+measureid+"_"+EmployeeAssessmentID)['0'].value;
				measurerating = $("#measurerating_"+measureid+"_"+EmployeeAssessmentID)['0'].value;
				measurecomment = $("#measurecomment_"+measureid+"_"+EmployeeAssessmentID)['0'].value;
			}else{
				measurename = $("#measurename_"+measureid)['0'].value;
				measuretarget = $("#measuretarget_"+measureid)['0'].value;
				measureweighting = $("#measureweighting_"+measureid)['0'].value;
				measurerating = $("#measurerating_"+measureid)['0'].value;
				measurecomment = $("#measurecomment_"+measureid)['0'].value;
			}
			
			var measure = {
			    "measureid": $(measure_el).data('measureid'),
			    "measurename": measurename,
			    "measuretarget": measuretarget,
			    "measureweighting": measureweighting,
			    "measurecomment": measurecomment,
			    "measurerating": measurerating,
			    "measurenew": measurenew
			}

			var submeasures = [];
			measure.submeasures = submeasures;
			submeasures_el = $("#measure_table_"+measureid+"_"+EmployeeAssessmentID+" .improvement_submeasure");
			submeasures_el.each(function(i,submeasure_el){
				submeasureid = $(submeasure_el).data('submeasureid');
				submeasurename = $("#submeasurename_"+submeasureid)['0'].value;
				submeasuretarget = $("#submeasuretarget_"+submeasureid)['0'].value;
				submeasureweighting = $("#submeasureweighting_"+submeasureid)['0'].value;
				var submeasure = {
				    "submeasurename": submeasurename,
				    "submeasuretarget": submeasuretarget,
				    "submeasureweighting": submeasureweighting
				}
				measure.submeasures.push(submeasure);
			});
			objective.measures.push(measure);
		});

		jsonData.objectives.push(objective);

	});
	console.log(JSON.stringify(jsonData));

	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/save_improvement_plan.cfm",
		data:{ 
			Objectives:JSON.stringify(jsonData),
			EmployeeAssessmentID:EmployeeAssessmentID,
			AssessmentPhaseID:AssessmentPhaseID,
			ObjectiveTypeID:ObjectiveTypeID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
    		$("#AdminToggel"+AdminID).removeClass("activated");
    		getEmployeeAssessments(AdminID,EmployeeAssessmentID,'',1);
        }
	});
}

function calcWeighting(EmployeeAssessmentID){
	objectives_el = $("#improvement_plan"+EmployeeAssessmentID+" .improvement_objective");
	objectives_el.each(function(i,el){
		objectiveid = $(el).data('objectiveid');
		objectivenew = $(el).data('objectivenew');
		new_objectiveweighting = 0;

		measures_el = $("#objective_"+objectiveid+"_"+EmployeeAssessmentID+" .improvement_measure");
		measures_el.each(function(i,measure_el){
			measurenew = $(measure_el).data('measurenew');
			measureid = $(measure_el).data('measureid');
			new_measureweighting = 0;

			submeasures_el = $("#measure_table_"+measureid+"_"+EmployeeAssessmentID+" .improvement_submeasure");
			submeasures_el.each(function(i,submeasure_el){
				submeasureid = $(submeasure_el).data('submeasureid');
				submeasureweighting = $("#submeasureweighting_"+submeasureid)['0'].value;
				new_measureweighting = (new_measureweighting-0) + (submeasureweighting-0);
			});

			if(measurenew == "0"){
				$("#measureweighting_"+measureid+"_"+EmployeeAssessmentID)['0'].value = new_measureweighting;
			}else{
				$("#measureweighting_"+measureid)['0'].value = new_measureweighting;
			}
			new_objectiveweighting = new_objectiveweighting + new_measureweighting;
		});

		if(objectivenew == "0"){
			$("#objectiveweighting_"+objectiveid+"_"+EmployeeAssessmentID)['0'].value = new_objectiveweighting;
		}else{
			
			$("#objectiveweighting_"+objectiveid)['0'].value = new_objectiveweighting;
		}
	});
}

function addImprovementReview(AdminID,EmployeeAssessmentID,ObjectiveTypeID){
	StartDate = $("#ReviewDate"+EmployeeAssessmentID+"_"+ObjectiveTypeID)['0'].value;
	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/add_review.cfm",
		data:{ 
			ObjectiveTypeID:ObjectiveTypeID,
			EmployeeAssessmentID:EmployeeAssessmentID,
			StartDate:StartDate
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
   			$("#AdminToggel"+AdminID).removeClass("activated");
   			if(ObjectiveTypeID == 2){
				getEmployeeAssessments(AdminID,EmployeeAssessmentID,1,1);
   			}else{
				getEmployeeAssessments(AdminID,EmployeeAssessmentID,'',1);
   			}
        }
	});
}


function saveProceduralImprovementReview(AdminID,EmployeeAssessmentID,ObjectiveTypeID,ReviewID){
	var jsonData = {};
	var submeasure_answers = [];
	jsonData.submeasure_answers = submeasure_answers;
	submeasure_answers_el = $(".submeasure_answer_"+ReviewID+"_"+EmployeeAssessmentID);
	
	submeasure_answers_el.each(function(i,el){
		SubMeasureID = $(el).data('submeasureid');
		submeasure_rating = $("#submeasure_rating_"+ReviewID+"_"+EmployeeAssessmentID+"_"+SubMeasureID)['0'].value;
		submeasure_comment = $("#submeasure_comment_"+ReviewID+"_"+EmployeeAssessmentID+"_"+SubMeasureID)['0'].value;
		console.log(submeasure_rating);
		console.log(submeasure_comment);
		var submeasure = {
		    "submeasureid": SubMeasureID,
		    "submeasure_rating": submeasure_rating,
		    "submeasure_comment": submeasure_comment
		}
		submeasure_answers.push(submeasure);
	});
	console.log(JSON.stringify(jsonData));

	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/save_procedual_review.cfm",
		data:{ 
			ObjectiveTypeID:ObjectiveTypeID,
			Submeasures:JSON.stringify(jsonData),
			EmployeeAssessmentID:EmployeeAssessmentID,
			ReviewID:ReviewID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
   			$("#AdminToggel"+AdminID).removeClass("activated");
			getEmployeeAssessments(AdminID,EmployeeAssessmentID,'',1);
        }
	});
}

function saveStrategicImprovementReview(AdminID,EmployeeAssessmentID,ObjectiveTypeID,ReviewID){
	var jsonData = {};
	var measure_answers = [];
	jsonData.measure_answers = measure_answers;
	measure_answers_el = $(".measure_answer_"+ReviewID+"_"+EmployeeAssessmentID);
	
	measure_answers_el.each(function(i,el){
		MeasureID = $(el).data('measureid');
		measure_rating = $("#measure_rating_"+ReviewID+"_"+EmployeeAssessmentID+"_"+MeasureID)['0'].value;
		measure_comment = $("#measure_comment_"+ReviewID+"_"+EmployeeAssessmentID+"_"+MeasureID)['0'].value;
		measure_improvement = $("#measure_improvement_"+ReviewID+"_"+EmployeeAssessmentID+"_"+MeasureID)['0'].value;
		measure_timeframe = $("#measure_timeframe_"+ReviewID+"_"+EmployeeAssessmentID+"_"+MeasureID)['0'].value;
		console.log(measure_rating);
		console.log(measure_comment);
		var measure = {
		    "measureid": MeasureID,
		    "measure_rating": measure_rating,
		    "measure_improvement": measure_improvement,
		    "measure_timeframe": measure_timeframe,
		    "measure_comment": measure_comment
		}
		measure_answers.push(measure);
	});
	console.log(JSON.stringify(jsonData));

	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/save_strategic_review.cfm",
		data:{ 
			ObjectiveTypeID:ObjectiveTypeID,
			Measures:JSON.stringify(jsonData),
			EmployeeAssessmentID:EmployeeAssessmentID,
			ReviewID:ReviewID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
   			$("#AdminToggel"+AdminID).removeClass("activated");
			getEmployeeAssessments(AdminID,EmployeeAssessmentID,1,1);
        }
	});
}

function addProceduralImprovement(AdminID,EmployeeAssessmentID){
	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/add_procedural_improvement.cfm",
		data:{ 
			EmployeeAssessmentID:EmployeeAssessmentID,
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
   			$("#AdminToggel"+AdminID).removeClass("activated");
			getEmployeeAssessments(AdminID,EmployeeAssessmentID,'',1);
        }
	});
}
function addStrategicImprovement(AdminID,EmployeeAssessmentID){
	$.ajax({
		method: "Post",
		url: "ajax/performance/performance_improvement/add_strategic_improvement.cfm",
		data:{ 
			EmployeeAssessmentID:EmployeeAssessmentID
		}
	}).done(function( data ) {
		response = JSON.parse(data);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
   			$("#AdminToggel"+AdminID).removeClass("activated");
			getEmployeeAssessments(AdminID,EmployeeAssessmentID,1,1);
        }
	});
}

