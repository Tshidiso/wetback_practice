
$(function(){

	$("#jobs").click(function(){
		$("#lists-holder").slideToggle("slow");
	})
})

function addJob(){
swal("Add New Job:", {
  content: "input",
}) .then((d) => {
    var job= d;
    
    if( job%1 === 0 && job!=null && job!="" ){

       $.ajax({
       	 type:'post',
         url: "ajax/jobs.cfm?job="  + job,
  	     });
         swal(`Job created: ${d}`)
         .then((value) => {
			 window.location.href = 'index.cfm';
			});
        }else{
	       swal({
	       	title: "Error",
		    text: "Job has to be a number",
		    icon: "error",
		    buttons: {
              confirm: {
                 value:"OK",
             },
           },

	       }) .then((value) => {
				 window.location.href = 'index.cfm';
				});
            }  
        }) 
    }


function showDetails(jobID){
      $.ajax({

        type:"Post",
        url:"ajax/job_details.cfm?ID="+jobID
      }).done( function(data){
          $("#details").html(data);
      })    
}