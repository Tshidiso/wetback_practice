<cfoutput>
<cfif session.error gt "">
    <div class="alert alert-danger alert-dismissible no-margin" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-exclamation-circle"></i> Error:</strong> #session.error#
        <cfset session.error = "">
    </div>
</cfif>
<cfif session.success gt "">
    <div class="alert alert-success alert-dismissible no-margin" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success:</strong> #session.success#
        <cfset session.success = "">
    </div>
</cfif>
</cfoutput>