<cfinclude template="head.cfm">
<cfinclude template="#application.check_session#">
<script src="js/performance.js"></script>
<body class="nav-active">
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<!-- header of the page -->
		<cfset page = "HR/IR">
		<cfset ModuleID = 2>
		<cfinclude template="header.cfm">
		<div id="main" role="main">
			<div class="main-holder">
				<div id="performance-holder">
				</div>
			</div>
			<!-- navigation panel -->
			<div class="left-panel">
				<a href="#" class="nav-opener text-uppercase"><span></span></a>
				<div class="heading-wrap padding-left-10">
					<strong class="panel-title text-capitalize left-text">HR/IR</strong>
				</div>
				<!-- search form -->
				<div class="clear-10">
				</div>
				<div id="lists-holder">
				<ul class="sub-navigation accordion list-unstyled">
						<li class="list-item active">
							<a href="#" class="opener">
								<i class="fa fa-list"></i> 
								Performance Manager
							</a>
							<div class="slide" style="display: block;">
								<ul class="second-nav list-unstyled">
									
									<li id="PerformanceAssesmentsLink" onclick="getPerformanceAssesments();">
										<p>
											Performance Assessments
										</p>
									</li>
									<cfinvoke component="#Application.permissions_component#" method="ModuleItem" returnvariable="ReturnVar">
									    <cfinvokeargument name="AdminID" value="#session.AdminID#">
									    <cfinvokeargument name="AdminTypeID" value="#session.AdminTypeID#">
									    <cfinvokeargument name="ModuleItemID" value="9">
									</cfinvoke>
									<cfif ReturnVar.ReturnCode eq 0>
										<li id="CreateAssesmentLink" onclick="getCreateAssesment();">
											<p>
												Manage Assessments
											</p>
										</li>
									</cfif>
								</ul>
							</div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
	
	<link 	href="css/performance.css" rel="stylesheet" type="text/css" defer></link>
	<script src="js/performance.js" type="text/javascript"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>

	<footer></footer>
</body>
</html>