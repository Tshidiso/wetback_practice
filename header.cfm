<cfoutput>
<header id="header">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href=""><img src="images/logo.png"></a>
			</div>
			<!-- user profile information block -->
			<div class="user-hold pull-right">
				<div class="profil-info dropdown pull-right">
					<div class="img-hold pull-right">
						<img src="images/profile_pics/#session.ProfilePicture#" id="imgProfilePicture">
					</div>
				</div>
				<div class="profil-info dropdown pull-right">
					
					<button type="button" id="menu-toggel" onclick="event.stopPropagation();$('##menu-holder').slideToggle();"  class="dropdown-toggle">
					#session.Firstname#
					#session.Surname#
					<span class="caret"><i class="icon-next"></i></span>
					</button>
					<a href="ajax/modals/password_reset.cfm" data-modal id="resetPasswordModal" class="hidden"></a>
					<ul class="dropdown-menu" id="menu-holder">
						<li><a href="ajax/modals/profile_edit.cfm" data-modal id="editProfileModal">Edit Profile</a></li>
						<li><a href="">Help</a></li>
						<li><a href="?logout=true">Sign Out</a></li>
					</ul>
				</div>
				<ul class="add-info pull-right list-unstyled hidden-xs">
					<!--- <li><a href=""><i class="icon-tick"></i></a></li> --->
					<li>
						<a onclick="$('##notifications-holder').slideToggle();$('##notification-badge').hide('scale');">
							<i class="icon-attention"></i> <span class="badge" id="notification-badge" style="display:none;">!</span>
						</a>
						<ul class="dropdown-menu notifications"  id="notifications-holder">
							<span class="no-notifications">No Notifications yet</span>
						</ul>
					</li>
				</ul>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav list-unstyled">
				    <li><a href="index.cfm">Projects</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</header>
</cfoutput>