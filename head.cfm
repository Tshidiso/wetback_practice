<!DOCTYPE html>
<html>
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Wetback</title>
	<link rel="icon" href="wetback1.ico" type="image/x-icon">
	<!-- include custom google fonts -->
	<!--- <link href='https://fonts.googleapis.com/css?family=Nunito:400,700' rel='stylesheet' type='text/css'> --->
	<!-- include the stylesheets -->

	<link media="all" rel="stylesheet" href="css/select2.css">
	<link media="all" rel="stylesheet" href="css/jquery.modal.min.css">
	<link media="all" rel="stylesheet" href="css/jquery.notifyBar.css">
	<link media="all" rel="stylesheet" href="css/font-awesome.css">
	<link media="all" rel="stylesheet" href="css/main.css">
	<link media="all" rel="stylesheet" href="css/fullcalendar.css">
	<link media="all" rel="stylesheet" href="css/bootstrap-material-datetimepicker.css">
	<link media="all" rel="stylesheet" href="css/icon.css">
	<link media="all" rel="stylesheet" href="css/shared.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/all.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!-- include the stylesheets -->
	<!-- include jQuery library -->

	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" defer></script> -->
	<script src="js/jquery-1.11.2.min.js"></script>
	<script src="js/select2.js"></script>
	<script src="js/jquery.ui.js" defer></script>
	<script src="js/bootstrap.min.js" defer></script>
	<script src="js/moment.min.js" defer></script>
	<script src="js/fullcalendar.min.js" defer></script>
	<script src="js/jquery.notifyBar.js" defer></script>
	<!-- include custom JavaScript -->
	<script src="js/jquery.modal.js" defer></script>
	<script src="js/jquery.mobile.touch.js" defer></script>
	<script src="js/bootstrap-material-datetimepicker.js" defer></script>
	<script src="js/jquery.main.js" defer></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="js/all.js"></script>
      

	<!-- include custom JavaScript -->
</head>