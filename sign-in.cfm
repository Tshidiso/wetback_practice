<cfparam name="url.action" default="">
<cfif session.adminid gt "">
  <cflocation url="index.cfm" addtoken="false">
</cfif>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <title>Sign In</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width">        
  <link rel="stylesheet" href="toolbox/css/all.css">
</head>
<body>
  <div id="main-wrapper">
    <div class="template-page-wrapper">
      <div class="container margin-top-30">
        <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-primary login-panel">
              <div class="panel-heading">
                <h3 class="panel-title">Sign in</h3>
              </div>
              <div class="panel-body">
                <cfif url.action eq "error">
                  <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fa fa-exclamation-circle"></i> Error:</strong> Invalid User Name / Password Combination
                </div>
                  
                </cfif>
                <form class="form-horizontal templatemo-signin-form" role="form" action="?" method="post">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="EmailAddress" class="col-sm-2 control-label"> Email:&nbsp;&nbsp; </label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="EmailAddress" placeholder="Email Address" name="EmailAddress" required>
                      </div>
                    </div>              
                  </div>
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="Password" class="col-sm-2 control-label">Password:</label>
                      <div class="col-sm-10">
                        <input type="Password" class="form-control" id="Password" placeholder="Password" name="Password" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12">
                      <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="Sign in" class="btn btn-default pull-right" name="login">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>    
    </div>
  </div>
  <footer class="templatemo-footer">
    <div class="templatemo-copyright">
            <img src="toolbox/images/fusebox.png" class="pull-right">
      <p>Copyright &copy; Fusebox</p>
    </div>
  </footer>
</div>
</body>
</html>