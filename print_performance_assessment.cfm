<cftry>
<cfoutput>
	<cfset token = createUUID()>
	<cfparam name="url.AdminID" default="">
	<cfparam name="url.EmployeeAssessmentID" default="">
	<cfparam name="url.AssessmentID" default="">
	<cfparam name="url.ObjectiveTypeID" default="">
	<cfparam name="IsManager" default="1">
	<cfparam name="IsAssessmentSummary" default="0">
	<cfparam name="TOTAL_PERCENT" default="0">
	<cfparam name="url.EmployeeAssessmentID" default="0">
	<cfparam name="IncAdminID" default="">
	<cfdocument format="PDF" filename="uploads\Assessment#token##url.EmployeeAssessmentID#.pdf" overwrite="Yes">
	<cfquery name="get_assessment" datasource="#application.db#">
		SELECT a.Name, ea.DateAdded, ea.AdminID,ea.CompleteDate,
		a.AssessmentID, ea.Complete, ea.ProceduralComplete, ea.StrategicComplete, 
		ea.EmployeeAssessmentID, ea.StartDate, 
		ap.Name as PhaseName,
		ap.Class,
		author.FirstName,
		author.Surname
		From EmployeeAssessments ea
		inner join Assessments a on ea.AssessmentID = a.AssessmentID
		inner join Admin author on ea.AddedByAdminID = author.AdminID
		inner join lk_AssessmentPhases ap on ap.AssessmentPhaseID = ea.AssessmentPhaseID
		Where ea.EmployeeAssessmentID = <cfqueryparam cfsqltype="cf_sql_integer" value="#EmployeeAssessmentID#">
	</cfquery>
	<cfset IncAdminID = get_assessment.AdminID>
	<cfset url.AssessmentID = get_assessment.AssessmentID>
	<cfset AssessmentID = get_assessment.AssessmentID>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
		<html>
		<head>
		    <title>Tasks</title>
		</head>
		<style type="text/css">
			<cfinclude template="css/main.css">
			<cfinclude template="css/performance.css">
			.assessment-header{
				text-align: center;
			}
			body{
				padding:20px;
			}
			.details-holder {
				width: 300px;
				font-size: 12px;
				margin-left: 400px;
			}
			.detail {
			    clear: both;
			    display: block;
			    min-height: 20px;
			}
			.detail-left {
			    float: left;
			    font-weight: bold;
			}
			.detail-right {
    			float: right;
			}
			.line{
				height: 2px;
				clear: both;
				background: ##000;
				width: 100%;
				margin-bottom: 20px;
				margin-top: 60px;
			}
			.small-line{
				height: 2px;
				clear: both;
				background: ##000;
				width: 100%;
				margin-bottom: 10px;
				margin-top: 10px;
			}
			.small-line2{
				height: 1px;
				clear: both;
				background: ##000;
				width: 100%;
				margin-bottom: 5px;
				margin-top: 5px;
			}
			.signature_line{
				width: 200px;
				height: 2px;
				background: ##000;
				margin-top: 25px;
			}
			tr td{
				text-align: left;
				padding:10px;
			}
			td.left-text{
				text-align: left;
				padding:5px;
				font-size: 14px;
			}
			td.right-text{
				text-align: right;
				
			}
			.percentage-bar-holder,.variation-blocks-holder{
				margin-top: 0px;
			}
			.percentage-holder{
				font-size: 12px;
			}
			.summary-sub-heading{
				margin-bottom: 5px;
				margin-top: 10px;
			}
			.percentage-bar,.variation-block{
				padding-top: 4px;
			}
			.variation-block{
				text-align: center;
			}
			table.no-pad-table{
				padding:0px;
			}
			.no-pad-table tr{
				padding: 0px;
			}
			.no-pad-table tr td{
				padding: 0px;
				width: 20px;
				height: 20px;
			}
			.variation-blocks-holder{
				padding-left: 20px;
			}
			.objective-name{
				text-align: left;
			}
			.no-pad-row{
				padding-top: 0px;
				padding-bottom: 0px;
			}
			.no-pad-row td{
				padding-top: 0px;
				padding-bottom: 0px;
			}
		</style>
		<body>
		    <img src="#Application.url#images/white-logo.png" style="width:150px;float:left;"> 
		    <div class="details-holder">
		     	<div class="detail">
		     		<div class="detail-left">DOCUMENT NO:</div> 
		     		<div class="detail-right">#url.EmployeeAssessmentID#</div>
		     	</div>
		     	<div class="detail">
		     		<div class="detail-left">DATE:</div> 
		     		<div class="detail-right">#DateFormat(get_assessment.StartDate,"dd mmmm yyyy")#</div>
		     	</div>
		     	<div class="detail">
		     		<div class="detail-left">AUTHOR:</div> 
		     		<div class="detail-right">#get_assessment.FirstName# #get_assessment.Surname#</div>
		     	</div>
		    </div>
		    <div class="small-line"></div>
		    <h4 style="width:100%;text-align:center;">#get_assessment.PhaseName# Assessment</h4>
		    <div class="small-line"></div>
			<cfquery name="get_assessments" datasource="#application.db#">
				SELECT 
				a.Name, ea.DateAdded, ea.AssessmentPhaseID,
				a.AssessmentID, ea.Complete, ea.ProceduralComplete, ea.StrategicComplete, 
				ea.EmployeeAssessmentID, 
				ea.StartDate, 
				ap.Name as PhaseName,
				ap.Class
				From Assessments a
				inner join EmployeeAssessments ea on ea.AssessmentID = a.AssessmentID
				inner join lk_AssessmentPhases ap on ap.AssessmentPhaseID = ea.AssessmentPhaseID
				Where ea.EmployeeAssessmentID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.EmployeeAssessmentID#">
				order by a.AssessmentID,ea.StartDate 
			</cfquery>
			<cfloop query="get_assessments">
			<cfset total_percent = 0>
				<cfinclude template="ajax/performance/performance_assessment/print_admin.cfm">	
				<!--- Assessment Summary --->
				<cfinclude template="ajax/performance/performance_assessment/print_assessment_summary.cfm">	
				<!--- Assessment Summary --->
			</cfloop>
		</body>
		</html>
	</cfdocument>
	<cfcontent type="application/pdf" file="#expandPath('.')#\uploads\Assessment#token##url.EmployeeAssessmentID#.pdf" deletefile="Yes">
</cfoutput>
<cfcatch type="any">
    <cfdump var="#cfcatch#">
</cfcatch>
</cftry>